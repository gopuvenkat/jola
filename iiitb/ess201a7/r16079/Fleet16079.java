package iiitb.ess201a7.r16079;
import java.util.*;
import iiitb.ess201a7.a7base.*;

public class Fleet16079 extends Fleet {
	ArrayList<Car> car=new ArrayList<Car>(); 
	private int no_of_car=1;
	public Fleet16079(String colour) {
		super(16079,colour);
	}
	@Override
	public void addCar(int speed) {
		car.add(new Car16079(160790+no_of_car,speed));
		this.no_of_car++;
	}
	public ArrayList<Car> getCars(){
		return car;
	}
	@Override
	public Car findNearestCar(Location loc) {
		double min_dist=99999;
		Car findcar=null;
		for(Car c: car){
			double temp=Math.sqrt(c.distSqrd(loc));
			if(min_dist>temp && c.getStatus()==Car.Idle){
				min_dist=temp;
				findcar=c;
			}
		}
		return findcar;
	}
}
