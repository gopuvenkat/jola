package iiitb.ess201a7.r16079;
import iiitb.ess201a7.a7base.*;

class Car16079 extends Car {
	private Location location=null;
	private Trip trip=null;
	private int status;
	public Car16079(int fid,int speed) {
		super(fid,speed);
		this.setStatus(Car.Idle);
	}

	@Override
	public void setLocation(Location l){
		this.location=l;
		// TODO Auto-generated method stub
	}
	public int distSqrd(Location loc) {
		double x1=loc.getX();
		double x2=this.getLocation().getX();
		double y1=loc.getY();
		double y2=this.getLocation().getY();
		double totaldist=Math.pow(y2-y1, 2)+Math.pow(x2-x1, 2);
		return (int)totaldist;
	}
	@Override
	public Location getLocation() {
		// TODO Auto-generated method stub
		return this.location;
	}

	@Override
	public void setStatus(int s) {
		this.status=s;
		// TODO Auto-generated method stub

	}

	@Override
	public int getStatus() {
		return this.status;
	}

	@Override
	public void assignTrip(Trip trip) {
		this.trip=trip;
		this.status = 2;
		// TODO Auto-generated method stub
	}
	public  Trip getTrip(){
		return this.trip;
	}

	@Override
	public Location getStart() {
		// TODO Auto-generated method stub
		return this.trip.getStart();
	}

	@Override
	public Location getDest() {
		// TODO Auto-generated method stub


		return this.trip.getDest();
	}

	@Override
	public void updateLocation(double deltaT) {
		if(this.getStatus()==Car.Booked){
			double x1=this.location.getX();
			double x2=trip.getStart().getX();
			double y1=this.location.getY();
			double y2=trip.getStart().getY();
			double dist=Math.sqrt(distSqrd(trip.getStart()));
			//System.out.println(dist);
			double final_x=x1+(deltaT*this.getSpeed()*(x2-x1)/dist);
			double final_y=y1+(deltaT*this.getSpeed()*(y2-y1)/dist);
			this.location.set((int)final_x,(int)final_y);
			if(dist<deltaT*getSpeed()){
				this.setStatus(Car.OnTrip);
			}
		}
		else if(this.getStatus()==Car.OnTrip){
			double x1=this.location.getX();
			double x2=trip.getDest().getX();
			double y1=this.location.getY();
			double y2=trip.getDest().getY();
			double dist=Math.sqrt(distSqrd(trip.getDest()));
		//	System.out.println(dist);
			double final_x=x1+(deltaT*this.getSpeed()*(x2-x1)/dist);
			double final_y=y1+(deltaT*this.getSpeed()*(y2-y1)/dist);
			this.location.set((int)final_x,(int)final_y);
			if(dist<deltaT*getSpeed()){
				this.setStatus(Car.Idle);
			}
		}
	}


}
