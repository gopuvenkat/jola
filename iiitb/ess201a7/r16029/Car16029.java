package iiitb.ess201a7.r16029;

import iiitb.ess201a7.a7base.*;
import java.util.*;

 public class Car16029 extends Car {
	int status;
	Trip trip;
    Location car_l=new Location(0, 0);
    public static int id=160291;
	
	public Car16029(int speed) {
		super(id,speed);
		status=1;
		id++;
	}

	@Override
	public void setLocation(Location l) {
		// TODO Auto-generated method stub
		car_l.set(l.getX(),l.getY());
	}

	@Override
	public Location getLocation() {
		// TODO Auto-generated method stub
		return car_l;
	}

	@Override
	public void setStatus(int s) {
		// TODO Auto-generated method stub
			status=s;		
	}

	@Override
	public int getStatus() {
		// TODO Auto-generated method stub
		return status;
		
	}

	@Override
	public void assignTrip(Trip trip) {
		// TODO Auto-generated method stub
		this.trip=trip;
		status=2;
	}

	@Override
	public Location getStart() {
		// TODO Auto-generated method stub
		return trip.getStart();
	}

	@Override
	public Location getDest() {
		// TODO Auto-generated method stub
		return trip.getDest();
	}
	
	public int distSqrd(Location loc) {
		return (int)(Math.pow(car_l.getX() - loc.getX(), 2) + Math.pow(car_l.getY() - loc.getY(), 2));
 
    }
	@Override
	public void updateLocation(double deltaT) {
		// TODO Auto-generated method stub
		Location l1, l2;
        double X, Y, d, D, cosT, sinT;
        if(this.getStatus() == 0){
            this.setStatus(1);
            return;
        }

        if(this.getStatus() == 2) {
            l1 = new Location(this.getLocation().getX(), this.getLocation().getY());
            l2 = new Location(this.getLocation().getX(), this.getLocation().getY());
            d = maxSpeed * deltaT;
            X = this.trip.getStart().getX() - this.car_l.getX();
            Y = this.trip.getStart().getY() - this.car_l.getY();
            D = Math.sqrt(Math.pow(X, 2) + Math.sqrt(Math.pow(Y, 2)));

           

            l2.set((int) (l2.getX() + d * (X/D)), (int) (l2.getY() + d * (Y/D)));
            this.setLocation(l2);

            if((l1.getX() <= trip.getStart().getX() && trip.getStart().getX() <= l2.getX())
                    || (l1.getX() >= trip.getStart().getX() && trip.getStart().getX() >= l2.getX())){
                this.setLocation(trip.getStart());
                this.setStatus(3);
            }
            return;
        }

        else if(this.getStatus() == 3){
            l1 = new Location(this.getLocation().getX(), this.getLocation().getY());
            l2 = new Location(this.getLocation().getX(), this.getLocation().getY());
            d = maxSpeed * deltaT;
            X = this.trip.getDest().getX() - this.car_l.getX();
            Y = this.trip.getDest().getY() - this.car_l.getY();
            D = Math.sqrt(Math.pow(X, 2) + Math.sqrt(Math.pow(Y, 2)));

         

            
            l2.set((int) (l2.getX() + d * (X/D)), (int) (l2.getY() + d * (Y/D)));
            this.setLocation(l2);

            if((l1.getX() <= trip.getDest().getX() && trip.getDest().getX() <= l2.getX())
                    || (l1.getX() >= trip.getDest().getX() && trip.getDest().getX() >= l2.getX())){
                this.setLocation(trip.getDest());
                this.setStatus(1);
            }
return;
        }
    }

	
public Trip getTrip(){
	return trip;
}

}
