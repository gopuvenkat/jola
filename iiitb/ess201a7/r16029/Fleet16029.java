package iiitb.ess201a7.r16029;



import iiitb.ess201a7.a7base.*;
import java.util.*;

public class Fleet16029 extends Fleet {
	ArrayList<Car16029> flee;
	public Fleet16029(String colour) {
		super(16029,colour);
		flee=new ArrayList<Car16029>();
	}

	@Override
	public void addCar(int speed) {
        Car16029 c=new Car16029(speed);
		flee.add(c);
	}



	@Override
	public Car findNearestCar(Location loc) {
	Car nearestCar = null;
        int min = 100000000;
        
        for(int i = 0; i < flee.size(); i++){
            if(flee.get(i).distSqrd(loc) <= min && flee.get(i).getStatus() == 1){
                min = flee.get(i).distSqrd(loc);
                nearestCar = flee.get(i);
            }
        }
        return nearestCar;
    }

    // v3 - added
    public ArrayList<Car16029> getCars(){
        return flee;
    }


}
