package iiitb.ess201a7.r16057;
import iiitb.ess201a7.a7base.*;
import static java.lang.Math.*;
class Car16057 extends Car
{
	private Location loc;
	private int stat;
	private Trip t=null;
	public Car16057(int id,int speed)
	{
		super(id,speed);
	}
	@Override
	public void setLocation(Location l)
	{
		this.loc=l;
	}
	@Override
	public Location getLocation()
	{
		return loc;
	}
	@Override
	public void setStatus(int s)
	{
		this.stat=s;
	}
	@Override
	public int getStatus()
	{
		return stat;
	}
	@Override
	public void assignTrip(Trip trip)
	{
		this.t=trip;
		this.stat=2;//once we assign trip the car status will be booked
	}
	public Trip getTrip()
	{
		return this.t;
	}
	@Override
	public Location getStart()
	{
		if(this.stat==2 || this.stat==3)
		{
			return t.getStart();
		}
		return null;
	}
	@Override
	public Location getDest()
	{
		if(stat==2 || stat==3)
		{
			return t.getDest();
		}
		return null;
	}
	@Override
	public int distSqrd(Location location)
	{
		return (int)((pow(loc.getX()-location.getX(),2))+(pow(loc.getY()-location.getY(),2)));
	}
	@Override
	public void updateLocation(double deltaT)
	{
		if(stat!=1)
		{
			double dist=deltaT*getSpeed();
			double x,y;
			Location l=new Location(0,0);
			if(stat==2)
			{
				double fd=sqrt(distSqrd(getStart()));
				if(dist<fd)
				{
					//changing the values of x and y using section formulae as we know ratio in which our point divides
					x=(((dist*getStart().getX())+((fd-dist)*loc.getX()))/fd);
					y=(((dist*getStart().getY())+((fd-dist)*loc.getY()))/fd);
					l.set((int)x,(int)y);
					setLocation(l);
				}
				else
				{
					setLocation(getStart());
					//changing the distance if the car dist in the time intraval exceeds start location dist is subtracted and found dist to be travelled
					dist=dist-fd;
					//as it reached start loc change its status to ontrip
					stat=3;
				}
			}
			if(stat==3)
			{
				double fd=sqrt(distSqrd(getDest()));
				if(dist<fd)
				{
					//changing the values of x and y using section formulae as we know ratio in which our point divides
					x=(((dist*getDest().getX())+((fd-dist)*loc.getX()))/fd);
					y=(((dist*getDest().getY())+((fd-dist)*loc.getY()))/fd);
					l.set((int)x,(int)y);
					setLocation(l);
				}
				else
				{
					//if the dist in intraval exceeds move that to destination
					setLocation(getDest());
					stat=1;
				}
			}
		}
	}
}
