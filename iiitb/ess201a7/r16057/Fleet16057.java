package iiitb.ess201a7.r16057;
import iiitb.ess201a7.a7base.*;
import java.util.ArrayList;
public class Fleet16057 extends Fleet
{
	private ArrayList<Car16057> list=new ArrayList<Car16057>();
	public Fleet16057(String colour)
	{
		super(16057,colour);
	}
	@Override
	public void addCar(int speed)
	{
		Location l=new Location(0,0);
		int iden=Integer.parseInt(Integer.toString(this.getId())+Integer.toString(1+list.size()));
		Car16057 car=new Car16057(iden,speed);
		car.setLocation(l);
		car.setStatus(1);
		list.add(car);
	}
	@Override
	public Car findNearestCar(Location loc)
	{
		//finding the nearest car in the fleet which is in idel state(compring distances)
		int dis=2147483647;
		Car16057 car=null;
		for (Car16057 c:list)
		{
			if(c.distSqrd(loc)<dis && c.getStatus()==1)
			{
				dis=c.distSqrd(loc);
				car=c;
			}
		}
		return car;
	}
	@Override
	public ArrayList<? extends Car> getCars()
	{
		return list;
	}
}
