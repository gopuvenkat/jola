package iiitb.ess201a7.r16001;

import iiitb.ess201a7.a7base.*;
import java.util.Random;

class Car16001 extends Car {

	private Location myLocation;
	private int myStatus;
	private Trip myTrip;
	private Random r;

	public Car16001(int fid, int speed) {
		super(fid, speed);
		maxSpeed = speed;
		myStatus = Idle;
		myTrip = null;
		r = new Random();
		myLocation = new Location(r.nextInt(1001), r.nextInt(1001));
	}

	@Override
	public int distSqrd(Location loc) {
		return ((myLocation.getX()-loc.getX())*(myLocation.getX()-loc.getX()) + (myLocation.getY()-loc.getY())*(myLocation.getY()-loc.getY()));
	}

	@Override
	public void setLocation(Location l) {
		myLocation = l;
	}

	@Override
	public Location getLocation() {
		return myLocation;
	}

	@Override
	public void setStatus(int s) {
		myStatus = s;
	}

	@Override
	public int getStatus() {
		return myStatus;
	}

	@Override
	public void assignTrip(Trip trip) {
		if(myStatus==Idle){
			setStatus(Booked);
			myTrip = trip;
		}
	}

	@Override
	public Location getStart() {
		return myTrip.getStart();
	}

	@Override
	public Location getDest() {
		return myTrip.getDest();
	}

	@Override
	public Trip getTrip() {
		return myTrip;
	}

	@Override
	public void updateLocation(double deltaT) {
		int x = myLocation.getX();
		int y = myLocation.getY();
		if(myStatus == Idle) {
			return;
		}
		else if(myStatus == Booked) {
			int xe = myTrip.getStart().getX();
			int ye = myTrip.getStart().getY();
			double dist = Math.sqrt(distSqrd(myTrip.getStart()));
			if(maxSpeed*deltaT >= dist) {
				setStatus(OnTrip);
				x = xe;
				y = ye;
			}
			else {
				x += ((xe-x)/dist)*maxSpeed*deltaT;
				y += ((ye-y)/dist)*maxSpeed*deltaT;
			}
		}
		else if(myStatus == OnTrip){
			int xe = myTrip.getDest().getX();
			int ye = myTrip.getDest().getY();
			double dist = Math.sqrt(distSqrd(myTrip.getDest()));
			if(maxSpeed*deltaT >= dist) {
				setStatus(Idle);
				x = xe;
				y = ye;
			}
			else {
				x += ((xe-x)/dist)*maxSpeed*deltaT;
				y += ((ye-y)/dist)*maxSpeed*deltaT;
			}
		}
		myLocation.set(x, y);
	}
}
