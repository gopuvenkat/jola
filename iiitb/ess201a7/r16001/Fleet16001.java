package iiitb.ess201a7.r16001;

import iiitb.ess201a7.a7base.*;
import java.util.ArrayList;

public class Fleet16001 extends Fleet {

	private ArrayList<Car> cars;
	int carID;

	public Fleet16001(String colour) {
			super(16001, colour);
			cars = new ArrayList<Car>();
			carID = 1;
	}

	@Override
	public void addCar(int speed) {
		String strI = Integer.toString(carID);
		String strFid = Integer.toString(getId());
		String strCid = strFid+strI;
		int resultID = Integer.parseInt(strCid);
		cars.add(new Car16001(resultID, speed));
		carID++;
	}

	@Override
	public Car findNearestCar(Location loc) {
		Double min = Double.POSITIVE_INFINITY;
		Car minCar = null;
		for(Car car: cars) {
			if(car.getStatus() == 1) {										// Idle = 1
				Double dist = Math.sqrt(car.distSqrd(loc));
				if(dist < min) {
					min = dist;
					minCar = car;
				}
			}
		}
		return minCar;
	}

	public ArrayList<? extends Car> getCars() {
		return cars;
	}

}
