package iiitb.ess201a7.r16118;
import java.util.*;
import java.io.*;
import iiitb.ess201a7.a7base.*;

public class Fleet16118 extends Fleet {
	int id = 161180;
	public Fleet16118(String colour) {
		super(16118, colour);
	}
	ArrayList<Car> carlist = new ArrayList<Car>();

	@Override
	public void addCar(int speed) {
		carlist.add(new Car16118(id,speed));
		id+=1;
	}

	@Override
	public Car findNearestCar(Location loc) {
		Car temp = null;
		double distance = -1;
		for(Car c:carlist){
			if(c.getStatus() == 1){
				distance = this.getDist(c.getLocation() , loc);
				temp = c;
				break;
			}
		}

		for(int i=0;i<carlist.size();i++){
			if(this.getDist(carlist.get(i).getLocation(), loc) < distance && this.carlist.get(i).getStatus() == 1){
				temp = carlist.get(i);
				distance = this.getDist(carlist.get(i).getLocation() , loc);
			}
		}
		return temp;
	}
	private double getDist(Location l1,Location l2){
		return Math.sqrt((l1.getX()-l2.getX())*(l1.getX()-l2.getX())+(l1.getY()-l2.getY())*(l1.getY()-l2.getY()));
	}

	@Override
	public ArrayList<Car> getCars(){
		return this.carlist;
	}
}
