package iiitb.ess201a7.r16118;
import java.util.*;
import java.io.*;
import iiitb.ess201a7.a7base.*;

class Car16118 extends Car {
	private static int random = 2;
	private Location location;
	private int status;
	private Trip t;


	public Car16118(int id,int speed) {
		super(id,speed);
		this.setLocation((new Location(random*2,random*3)));
		this.setStatus(1);
		random += 2;
	}

	@Override
	public void setLocation(Location l) {
		this.location = l;
	}

	@Override
	public Location getLocation() {
		return this.location;
	}

	@Override
	public void setStatus(int s) {
		this.status = s;
	}

	@Override
	public int getStatus() {
		return this.status;
	}

	@Override
	public int distSqrd(Location loc) {
		return (int)Math.pow(this.getLocation().getX() - loc.getX(), 2) + (int)Math.pow(this.getLocation().getY() - loc.getY(), 2);
	}

	@Override
	public void assignTrip(Trip trip) {
		this.t = trip;
		this.status = 2;
	}

	@Override
	public Location getStart() {
		return (t.getStart());
	}

	@Override
	public Location getDest() {
		return(t.getDest());
	}

	@Override
	public Trip getTrip() {
		return(this.t);
	}

	@Override
	public void updateLocation(double deltaT) {
		double initx = this.getLocation().getX();
		double inity = this.getLocation().getY();
		/*if(getStatus()==1){
			return ;
		}*/
		if(this.getStatus() == 2){
			double hyp = Math.sqrt(distSqrd(this.getStart()));
			double cos = (this.getStart().getX() - this.getLocation().getX())/hyp;
			double sin = (this.getStart().getY() - this.getLocation().getY())/hyp;
			initx = this.location.getX() + deltaT*this.getSpeed()*cos;
			inity = this.location.getY() + deltaT*this.getSpeed()*sin;
	        if(distSqrd(new Location((int)initx,(int)inity))>distSqrd(this.getStart())){
	          initx = this.getStart().getX();
	          inity = this.getStart().getY();
	        }
		}
		if(this.getStatus() == 3){
			double hyp = Math.sqrt(distSqrd(this.getDest()));
			double cos = (this.getDest().getX() - this.getLocation().getX())/hyp;
			double sin = (this.getDest().getY() - this.getLocation().getY())/hyp;
			initx = this.getLocation().getX() + deltaT*this.getSpeed()*cos;
			inity = this.getLocation().getY() + deltaT*this.getSpeed()*sin;
			if(distSqrd(new Location((int)initx, (int)inity)) > distSqrd(this.getDest())){
				initx = this.getDest().getX();
				inity = this.getDest().getY();
			}

		}
		location.set((int)initx,(int)inity);

		if (this.getStatus()==2 && this.getLocation().getX()==this.getStart().getX() && this.getLocation().getY()==this.getStart().getY()){
			this.setStatus(3);
		}
		if(this.getStatus()==3 && this.getLocation().getX()==this.getDest().getX() && this.getLocation().getY()==this.getDest().getY()){
			this.setStatus(1);
		}
	}

}
