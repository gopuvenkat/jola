# jola

A Taxi Aggregators tool implemented in Java<br>
Done as a part of the Java course at IIIT Bangalore

## To Compile and Run

```javac iiitb/ess201a7/a7base/*.java && javac iiitb/ess201a7/r16001/*.java && javac iiitb/ess201a7/r16118/*.java  && javac iiitb/ess201a7/r16099/*.java  && javac iiitb/ess201a7/r16079/*.java && javac iiitb/ess201a7/r16029/*.java && javac iiitb/ess201a7/r16057/*.java  && javac *.java && java Demo```
