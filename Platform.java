import java.util.ArrayList;

import iiitb.ess201a7.a7base.*;

public class Platform {

	private ArrayList<Fleet> fleets;

	public Platform() {
		fleets = new ArrayList<Fleet>();
	}

	public void addFleet(Fleet f) {
		fleets.add(f);
	}

	// for a request defined as a Trip, find the best car by checking each of its fleets
	// and assigns the car to this trip
	public Car assignCar(Trip trip) {
		Car sCar = null;
		Location start = trip.getStart();
		Double min = Double.POSITIVE_INFINITY;
		for(Fleet f: fleets) {
			Car tmp = f.findNearestCar(start);
			if(tmp != null) {
				Double dist = Math.sqrt(tmp.distSqrd(start));
				if(dist < min) {
					min = dist;
					sCar = tmp;
				}
			}
		}
		if(sCar != null)
			sCar.assignTrip(trip);
		return sCar;
	}

	// returns list of all cars (in all the fleets) managed by this platform
	public ArrayList<Car> findCars() {
		ArrayList<Car> cars = new ArrayList<Car>();
		for(Fleet f: fleets) {
			for(Car car: f.getCars()) {
				cars.add(car);
			}
		}
		return cars;
	}
}
